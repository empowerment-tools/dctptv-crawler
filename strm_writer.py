# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.


# -----------------------------------------------------------------------------------------------


__all__ = ["write_strm", "write_nfo", "write_playlist"]

# -----------------------------------------------------------------------------------------------

import json
import os
import sys
from datetime import datetime

from dctptv_tools import get_datetime, get_url, parse_json_input

# -----------------------------------------------------------------------------------------------

USER_AGENT = (
    "User-Agent=Mozilla/5.0 (Windows NT 6.1; rv:80.0) Gecko/20100101 Firefox/80.0"
)


NFO_PATTERN = """<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<movie>
    <title>%(title)s<</title>
    <outline>%(outline)s</outline>
    <plot>%(description)s</plot>
    <runtime>%(duration)d</runtime>
    <genre>interview</genre>
    <director>Kluge</director>
    <studio>dctp.tv</studio>
    %(thumbs)s
    %(tags)s
    %(premiered)s
</movie>"""

# -----------------------------------------------------------------------------------------------


# -----------------------------------------------------------------------------------------------


def create_fname(jitem):
    date = get_datetime(jitem)
    date_str = date.strftime("%Y-%m-%d") if date is not None else "NODATE"
    return f"{date_str}_{jitem['slug']}_{jitem['uuid']}"


# -----------------------------------------------------------------------------------------------


def write_strm(jitem, target_dir):
    # print(jitem)
    fname = os.path.join(target_dir, create_fname(jitem) + ".strm")

    with open(fname, "w") as fout:
        fout.write(get_url(jitem) + "|" + USER_AGENT + "\n")


# -----------------------------------------------------------------------------------------------


def create_tags(jitem):
    pattern = "<tag>%s</tag>"
    if len(jitem["tags"]) > 0:
        tags = [pattern % x for x in jitem["tags"]]
        tags = "\n".join(tags)
    else:
        tags = pattern % ""
    return tags


def create_thumbs(jitem):
    pattern = """<thumb aspect="set.poster" preview="%s">%s</thumb>"""
    if len(jitem["images"]) > 0:
        thumbs = [pattern % (x["url"], x["url"]) for x in jitem["images"]]
        thumbs = "\n".join(thumbs)
    else:
        thumbs = pattern % ""
    return thumbs


def parse_date(jitem):
    dtime = get_datetime(jitem)

    if dtime is not None:
        dtime = dtime.strftime("<premiered>%Y-%m-%d</premiered>")
    else:
        dtime = ""

    return dtime


def create_description(jitem):
    return jitem.get("description", jitem.get("teaser", ""))


def write_nfo(jitem, target_dir):
    x = jitem
    prop_selection = dict(
        [
            ("title", x["title"]),
            ("outline", x["subtitle"]),
            ("description", create_description(jitem).strip()),
            ("duration", x["duration_in_ms"] // 60000),
            ("thumbs", create_thumbs(jitem)),
            ("tags", create_tags(jitem)),
            ("identifier", create_fname(jitem)),
            ("premiered", parse_date(jitem)),
        ]
    )

    fname = os.path.join(target_dir, prop_selection["identifier"] + ".nfo")

    with open(fname, "w") as fout:
        fout.write(NFO_PATTERN % prop_selection)


# -----------------------------------------------------------------------------------------------


def write_playlist(playlist_path, playlist, strm_path):
    with open(playlist_path, "w") as fout:
        fout.write("#EXTM3U\n\n")

        for key, jitem in playlist.items():
            fout.write(
                "#EXTINF:%d, %s - %s\n"
                % (jitem["duration_in_ms"] // 1000, jitem["title"], jitem["subtitle"])
            )
            fpath = os.path.join(strm_path, create_fname(jitem) + ".strm")
            fout.write(fpath + "\n")


# -----------------------------------------------------------------------------------------------


def update_playlist(playlist_name, playlistdict, uuid, m):
    playlist = playlistdict.get(playlist_name, dict())
    playlist[uuid] = m
    playlistdict[playlist_name] = playlist


def build_playlistdict(moviedb):
    playlistdict = dict()

    for uuid, m in moviedb.items():
        play_list_titles = [
            pl["slug"] for pl in m["contained_in_playlists"] if "slug" in pl
        ]
        for playlist in play_list_titles:
            update_playlist(playlist, playlistdict, uuid, m)

    return playlistdict


if __name__ == "__main__":
    if len(sys.argv) > 1:
        with open(sys.argv[1], "r") as fin:
            moviedb = parse_json_input(fin)
    else:
        moviedb = parse_json_input(sys.stdin)

    # print( len(moviedb) )
    playlists = build_playlistdict(moviedb)
    # print( [ (k,len(playlists[k])) for k in playlists ] )
    print(len(playlists))

    for jitem in moviedb.values():
        write_strm(jitem, "strms")
        write_nfo(jitem, "strms")

    for pname, plist in playlists.items():
        # print(pname)
        write_playlist(
            os.path.join("topics", "%02d_%s.strm" % (len(plist), pname)),
            plist,
            "../strms",
        )
