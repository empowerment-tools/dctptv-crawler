#!/bin/sh

URL="http://www.dctp.tv"
REJECT_LIST="png,jpg,txt,css,pdf,undefined"
TARGET_DIR="htmls"

USER_AGENT="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0"



wget --mirror -e robots=off \
     --no-cookies \
     --no-hsts \
     --compression=auto \
     --waitretry=120 -t 50 --retry-connrefused \
     --random-wait \
     --directory-prefix="${TARGET_DIR}" \
     --user-agent="${USER_AGENT}" \
     --no-directories \
     --adjust-extension \
     --reject ${REJECT_LIST} \
     ${URL}


exit 0

