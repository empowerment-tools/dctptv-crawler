# dctptv-crawler

Scripte zum crawlen der [dctp.tv](https://www.dctp.tv) Mediathek und zur Erzeugung von playlists.

## Benötigte Tools

* Python 3
* [bash](https://www.gnu.org/software/bash)
* [wget](https://www.gnu.org/software/wget)
* [sed](https://www.gnu.org/software/sed)

## 1. crawle [dctp.tv](https://www.dctp.tv)

```console
$ ./download_htmls.sh
```

Erzeugt ein Verzeichnis "htmls" und befüllt es mit von [dctp.tv](https://www.dctp.tv) herunter geladenen HTML Dokumenten.

## 2. MediathekView Filmliste

```console
$ ./gen_mediathekview_list.sh
```
erzeugt dann aus den HTML Dokumenten die Datei ``dctptv.list.xz``, welche von der [MediathekView](https://mediathekview.de)-Anwendung eingelesen werden kann:

![Einstellungen](MediathekView_Screenshot.png)

## 3. strm+nfo Dateien

```console
$ ./gen_strm_playlists.sh
```

erzeugt die Unterordner ``strms`` und  ``topics``. 
``strms`` beinhaltet eine playlist (.strm) sowie eine Metadaten- (.nfo) Datei für jeden auf [dctp.tv](https://www.dctp.tv) gefundenen Film.
``topics`` enthält Themenschleifenaggregate diese Filme.

Die Themenschleifen lassen sich gut unter [Kodi](https://kodi.tv) abspielen.
