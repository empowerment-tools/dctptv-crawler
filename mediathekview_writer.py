# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.


# -----------------------------------------------------------------------------------------------

import itertools as it
import os
import lzma
from datetime import datetime
from datetime import time

from dctptv_tools import get_url, get_duration, get_datetime, parse_json_input

# -----------------------------------------------------------------------------------------------

HEADER='"Filmliste":["22.12.2020, 10:16","18.12.2020, 09:16","3","MSearch [Vers.: 3.1.144]","d0566e314eca9f30a0f1bab5be718daa"]'
#HEADER = HEADER + ',"Filmliste":["Sender","Thema","Titel","Datum","Zeit","Dauer","Größe [MB]","Beschreibung","Url","Website","Url Untertitel","Url RTMP","Url Klein","Url RTMP Klein","Url HD","Url RTMP HD","DatumL","Url History","Geo","neu"]'
#HEADER = ""
# Dauer: "00:28:45"


FILMLIST_ENTRY=""""X":[
"dctp.tv",
"%(thema)s",
"%(title)s",
"%(date)s",
"%(time)s",
"%(duration)s",
"",
"%(description)s",
"%(url)s",
"https://www.dctp.tv",
"",
"",
"",
"",
"",
"",
"%(timestamp)d",
"",
"",
"true"]"""

# -----------------------------------------------------------------------------------------------

def create_entry_dict( jitem ):

    dtime = get_datetime( jitem )
    if dtime is None:
        date = ""
        time = ""
        tstamp = 0
    else:
        date = dtime.strftime("%d.%m.%Y")
        time = dtime.strftime("%H:%M:%S")
        tstamp = int(dtime.timestamp())
        
    entry = dict( url = get_url( jitem ),
                title = jitem["title"].strip().strip("\"").replace("\"", "'"),
             duration = get_duration( jitem ).strftime("%H:%M:%S"),
                 date = date,
                 time = time, 
            timestamp = tstamp,
          description = jitem["description"].strip().strip("\"").replace("\"", "'"),
                thema = "" )
    
    if len( jitem["contained_in_playlists"] ) > 0:
        entries = list()
        for pls in jitem["contained_in_playlists"]:
            n_entry = entry.copy()
            n_entry["thema"] = pls["title"].strip().strip("\"").replace("\"", "'")
            entries.append( n_entry )
    else:
        entries = [entry]
    
    entries
    
    return entries

# -----------------------------------------------------------------------------------------------

def write_film_list( film_entries ):

    with lzma.open("dctptv.list.xz", "wt") as fout:
        fout.write("{")
        fout.write(HEADER)
        fout.write(",")
        fout.write( ",".join(film_entries) )
        fout.write("}")



# -----------------------------------------------------------------------------------------------

if __name__ == '__main__':
    # get movie entries
    moviedb = parse_json_input()
    #film_entries = [ (FILMLIST_ENTRY % create_entry_dict(jitem)).replace("\n"," ") for jitem in moviedb.values() ]
    film_entries = it.chain( *list(create_entry_dict(jitem) for jitem in moviedb.values()) )
    film_strs = ( (FILMLIST_ENTRY % x).replace("\n","").replace("\r","") for x in film_entries )
    write_film_list( film_strs )




