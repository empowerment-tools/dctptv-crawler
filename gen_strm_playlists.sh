#!/bin/bash

TARGET_DIR="htmls"

mkdir topics 2> /dev/null
mkdir strms 2> /dev/null


grep -l "__NEXT_DATA__" ${TARGET_DIR}/*.html | while read fn
do  
     #trgfn=${fn/\.html/.json}
     grep uuid < ${fn} | sed -e 's/.*<script id=\"__NEXT_DATA__.*>{/{/g' | sed -e 's/}<\/script>.*$/}/g' 
done | python3 strm_writer.py


exit 0
