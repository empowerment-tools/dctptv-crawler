# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.

# -----------------------------------------------------------------------------------------------


__all__ = ["get_url", "get_duration", "get_datetime", "parse_json_input"]

# -----------------------------------------------------------------------------------------------

import json
import sys
from datetime import datetime, time

URL_PATTERN = "https://cdn-segments.dctp.tv/%s_dctp_%s.m4v/playlist.m3u8"


# -----------------------------------------------------------------------------------------------


def get_url(jitem):
    if jitem["is_wide"] is True:
        infix = "720p"
    else:
        infix = "0500_4x3"

    return URL_PATTERN % (jitem["uuid"], infix)


def get_duration(jitem):
    duration = jitem["duration_in_ms"]

    hourd = 60 * 60 * 1000
    hour = duration // hourd

    minuted = 60 * 1000
    minute = (duration - (hour * hourd)) // minuted

    secondd = 1000
    second = (duration - (hour * hourd) - (minute * minuted)) // secondd

    return time(hour=hour, minute=minute, second=second)


def get_datetime(jitem):
    dtime = None

    for ttag in ["web_airdate", "airdate"]:
        if (ttag in jitem) and (len(jitem[ttag]) > 0):
            # "2011-05-02"
            dtime = datetime.strptime(jitem[ttag], "%Y-%m-%d")

    for ttag in ["updated", "created"]:
        if ttag in jitem:
            if len(jitem[ttag]) > 0:
                # "2009-04-16T11:13:03"
                dtime = datetime.strptime(jitem[ttag], "%Y-%m-%dT%H:%M:%S")

    return dtime


# -----------------------------------------------------------------------------------------------


def parse_json_input(inputf):
    moviedb = dict()

    for moviejs in inputf:
        # moviejs.keys()
        try:
            moviejs = json.loads(moviejs)
            media = moviejs["props"]
            media = media["pageProps"]
            media = media["media"]
            moviedb[media["uuid"]] = media
        except json.decoder.JSONDecodeError as e:
            # print(e)
            pass
        except KeyError as e:
            # print(e)
            pass

    return moviedb
